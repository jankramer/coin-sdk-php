<?php

namespace coin\sdk\np\messages\v1;

use Spatie\Enum\Enum;

/**
 * @method static self ALL()
 * @method static self UNCONFIRMED()
 */
class ConfirmationStatus extends Enum {}
